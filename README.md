A game made personally by me using Godot 3.1. 

This project was initially made due to the mid-test assignment by CSUI2019. 

Now, it's been published to www.itch.io/jam/1-week-game-jam-csui-2019
Feel free to download.

Credit goes to:
- Kenney (www.kenney.nl) for the font assets
- Elthen (https://elthen.itch.io/) for the character assets