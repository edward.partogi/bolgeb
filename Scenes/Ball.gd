extends KinematicBody2D

# Declare member variables here. Examples:
const THROW_VELOCITY = Vector2(800, -400)

var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += PlayLevel.GRAVITY * delta
	var colliding = move_and_collide(velocity * delta)
	if colliding:
#		if colliding.collider.has_method("hit"):
#			colliding.collider.hit()
		_on_impact(colliding.normal)

func launch(direction):
	velocity = THROW_VELOCITY * Vector2(direction, 1)

func _on_impact(normal : Vector2):
	velocity = velocity.bounce(normal)
	velocity *= 0.8
	
