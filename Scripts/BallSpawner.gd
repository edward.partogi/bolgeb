extends Position2D

# Declare member variables here. Examples:
const Ball_PS = preload("res://Scenes/BallR.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Area2D_body_exited(body):
	if body.position.x < 0 or body.position.x > 1920 or body.position.y < 0 or body.position.y > 1024:
		body.queue_free()
		var newBall = Ball_PS.instance()
		add_child(newBall)
