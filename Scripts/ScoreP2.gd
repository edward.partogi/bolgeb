extends Label

# Declare member variables here. Examples:
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	text = "Score : " + str(score)

func add_score():
	score += 1

func _on_Character_got_hit():
	add_score()