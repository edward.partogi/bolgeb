extends Node2D

# Declare member variables here. Examples:
const Ball_PS = preload("res://Scenes/BallR.tscn")

onready var ball1 = Ball_PS.instance()
onready var ball2 = Ball_PS.instance()

# Called when the node enters the scene tree for the first time.
func _ready():
	spawn_ball()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func spawn_ball():
	ball1 = Ball_PS.instance()
	add_child(ball1)
	ball1.deco = true
	ball1.att_state = true
	ball1.transform.origin = Vector2(78+rand_range(-2, 2), -60)
	
	ball2 = Ball_PS.instance()
	add_child(ball2)
	ball2.deco = true
	ball2.att_state = true
	ball2.transform.origin = Vector2(405+rand_range(-2, 2), -60)

func _on_Timer_timeout():
	spawn_ball()
