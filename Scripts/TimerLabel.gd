extends Label

onready var timerNode = get_parent().get_parent().get_node("RoundTimer")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	text = "TIME\n" + str(int(timerNode.get_time_left()))
