extends CanvasLayer

var mainText = ""

onready var winner = get_parent().winner
# Called when the node enters the scene tree for the first time.
func _ready():
	if winner == 0:
		mainText = "Everybody wins!"
	else:
		mainText = "P" + str(get_parent().winner) + " is the winner"
	$GameOverLabel.text = mainText

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Credits_pressed():
	get_tree().change_scene(str("res://Scenes/Credits.tscn"))

func _on_Exit_pressed():
	get_tree().quit()

func _on_Restart_pressed():
	get_tree().change_scene(str("res://Scenes/PlayLevel.tscn"))
