extends Area2D

signal caught
signal let_loose

onready var anim_player = get_node("AnimationPlayer")
onready var timer = get_node("Timer")

var showing = 0
var catch = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	anim_player.play("Show_Up")

func _on_Timer_timeout():
	if catch:
		hide()
		catch = false
		emit_signal("let_loose")
	else:
		set_process(true)
		anim_player.play("Show_Up")

func hide():
	showing = 0
	timer.start()
	anim_player.play("Hide")
	set_process(false)

func _on_AnimationPlayer_animation_finished(anim_name):
	if showing == 3 and anim_name == "Show_Up":
		hide()
	else:
		showing += 1


func _on_PapaTrap_body_entered(body):
	if body.has_method("_on_PapaTrap_caught"):
		body.caught = true
		emit_signal("caught")
		catch = true
		anim_player.stop()
		timer.start(5)
		set_process(false)