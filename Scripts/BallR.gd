extends RigidBody2D

const THROW_VELOCITY = Vector2(800, -400)

export (bool) var deco = false

export var att_state = false
onready var normal_ball = preload("res://Assets/Sheets/Ball/Ball.png")
onready var att_ball = preload("res://Assets/Sheets/Ball/BallAtt.png")

var bouncing = 0
var bouncingTimer = 0

func _ready():
	set_physics_process(false)

func _physics_process(delta):
	bouncingTimer += 1
	
	if bouncingTimer/10000000000000000000 == 0 and bouncing == 20 and att_state:
		bouncingTimer = 0
		print("point")
		change_state()

func change_state():
	if !deco:
		if att_state:
			att_state = false
			$Sprite.set_texture(normal_ball)
		else:
			att_state = true
			$Sprite.set_texture(att_ball)

func launch(direction):
	change_state()
	var velocity = THROW_VELOCITY * Vector2(direction, 1)
	apply_impulse(Vector2(0, 0), velocity)
	set_physics_process(true)

func _on_Ball_body_entered(body):
	if body.has_method("hit"):
		if att_state:
			body.hit()
			change_state()
		elif !body.holding and !att_state and body.state_priority != 7:
			body.holding = true
			queue_free()
	if body.get_class() == 'TileMap':
		bouncing += 1

func _on_VisibilityNotifier2D_screen_exited():
	if deco:
		queue_free()