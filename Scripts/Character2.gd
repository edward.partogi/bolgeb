extends KinematicBody2D

signal got_hit()

export (bool) var deco = false
export (int) var speed = 400
export (int) var jump_speed = -600

const UP = Vector2(0,-1)
const Ball_PS = preload("res://Scenes/BallR.tscn")

var velocity = Vector2()
var direction = 1
var held_item = null
var holding = false
var caught = false

onready var anim_player = $AnimationPlayer
onready var state_priority = 0 # idle : 0, run : 1, jump : 2

func _ready():
	anim_player.play("Idle")

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('ui_up2') && is_on_floor():
		jump()
	if Input.is_action_pressed('ui_right2'):
		facing(true)
		run(true)
	if Input.is_action_pressed('ui_left2'):
		facing(false)
		run(false)
	if Input.is_action_pressed('ui_down2'):
		duck()
	if Input.is_action_just_released('ui_shoot2'):
		if state_priority < 3 and holding:
			state_priority = 3
			anim_player.play("Throw")

func anim_restraint():
	if Input.is_action_just_released('ui_right2') or Input.is_action_just_released('ui_left2'):
		if state_priority < 2:
			state_priority = 0
	if Input.is_action_just_released('ui_down2'):
		if state_priority < 7:
			anim_player.play("Duck_backw")
			state_priority = 5

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Duck':
		state_priority = 6
		anim_player.stop()
	else:
		state_priority = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += delta * PlayLevel.GRAVITY
	if !deco:
		get_input()
	anim_restraint()
	if state_priority == 0:
		anim_player.play("Idle")
	
	velocity = move_and_slide(velocity, UP)
#	velocity = move_and_slide(velocity, UP, false, 4, PI/4, false)
#	for index in get_slide_count():
#		var collision = get_slide_collision(index)
#		if collision.collider.is_in_group('balls'):
#			collision.collider.apply_central_impulse(-collision.normal * 50)

func facing(right):
	if state_priority != 3:
		if !right:
			if direction == 1:
				direction = -1
				apply_scale(Vector2(-1, 1))
		else:
			if direction == -1:
				direction = 1
				apply_scale(Vector2(-1, 1))

func jump():
	if state_priority < 5 or state_priority == 7:
		if state_priority < 3:
			anim_player.play("Jump")
			state_priority = 2
		velocity.y = jump_speed

func run(right):
	if state_priority < 5 or state_priority == 7:
		if right:
			velocity.x += speed
		else:
			velocity.x -= speed
		if state_priority < 2:
			state_priority = 1
			anim_player.play("Run")

func duck():
	if state_priority < 6:
		state_priority = 5
		anim_player.play("Duck")

func hit():
	if state_priority < 7:
		state_priority = 7
		anim_player.play("Hit")
		emit_signal("got_hit")

func throw():
	held_item = Ball_PS.instance()
	get_parent().add_child(held_item)
	held_item.set_global_position($throwPos.get_global_position())
	held_item.launch(direction)
#		held_item.add_to_group('balls')
	held_item = null
	holding = false

#func caught():
#	deco = true
#	velocity.x = 0
#	velocity.y = 0
#
#func not_caught():
#	deco = false

func _on_PapaTrap_caught():
	if caught:
		deco = true
		velocity.x = 0
		velocity.y = 0

func _on_PapaTrap_let_loose():
	caught = false
	deco = false
