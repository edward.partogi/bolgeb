extends Node2D

const GameOver_PS = preload("res://Scenes/GameOver.tscn")
var total_ball = 2

export (int) var GRAVITY = 1200
onready var curr_scene = get_tree().current_scene
#onready var timer = $GUI/RoundTimer
var winner

func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	print(total_ball)

func finalWinner():
	if $GUI/ScoreP1.score > $GUI/ScoreP2.score:
		winner = 1
	elif $GUI/ScoreP1.score < $GUI/ScoreP2.score:
		winner = 2
	else:
		winner = 0

func _on_RoundTimer_timeout():
	$Character.deco = true
	$Character2.deco = true
	finalWinner()
	$RoundTimer.stop()
	var endGameLayer = GameOver_PS.instance()
	add_child(endGameLayer)
